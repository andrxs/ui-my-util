axfDirectivesDirectives.constant('printConfig', {
  URL_JZEBRA: '../../static/i2-connection/lib/applet/jzebra.jar',
  URL_PDF_RENDER: '../../static/i2-connection/lib/applet/lib/PDFRenderer-0.9.1.jar',
  NOTIFY_MESSAGE: 'Printer $0 not found',
  NOTIFY_HEADER: 'Alert'
})
.factory('printFactory', ['$timeout','dialogs','printConfig','$log',function (timeout,dialogs,printConfig,$logger) {	
	var service = {};
	var _applet,
		_uniqueId;

	service.setApplet = function(appletId){		
		_applet = document.getElementById(appletId);		
	};

	service.getUniqueId = function() {
		_uniqueId = 'appletJZebra-'+ Math.floor(Math.random() * 10000);

		return _uniqueId;
	};

	service.findPrinter = function(printerName){	
		if (_applet !== null) {	            	
	       	_applet.findPrinter(printerName);
	       	this.monitorFinding(_applet);
	   	}
	};

	service.printPDF = function(printOptions, appletId) {	
		service.setApplet(appletId);

		if (_applet === null || angular.isUndefined(_applet)) {	  
			$logger.error('[Applet is null or undefined.]');
			return;
		} 
		_applet.appendPDF(printOptions.url);  
		 while (!_applet.isDoneAppending()) {}
		
	    var printerName =  printOptions.store + _applet.getPrinterType();

		_applet.findPrinter(printerName);
		while (!_applet.isDoneFinding()) {}
		if (_applet.getPrinter() === null) {
			dialogs.notify(printConfig.NOTIFY_HEADER,printConfig.NOTIFY_MESSAGE.replace('$0',printerName));
			return;
		}

		_applet.printPS();	 
	};

	service.printPdfTmp = function(printOptions, appletId) {	
		service.setApplet(appletId);

		if (_applet === null || angular.isUndefined(_applet)) {	  
			$logger.error('[Applet is null or undefined.]');
			return;
		} 

		_applet.findPrinter(printOptions.printerName);
		while (!_applet.isDoneFinding()) {}
		if (_applet.getPrinter() === null) {
			dialogs.notify(printConfig.NOTIFY_HEADER,printConfig.NOTIFY_MESSAGE.replace('$0',printOptions.printerName));
			return;
		}

		var isDuplex = angular.isUndefined(printOptions.isDuplex) ? false : printOptions.isDuplex; 
		_applet.setDuplex(isDuplex);	
		
		var numberCopies = angular.isUndefined(printOptions.numberCopies) ? 0 : printOptions.numberCopies; 
		if(numberCopies > 0){
			_applet.setCopies(numberCopies);
		}
		
	    _applet.appendPDF(printOptions.url);

	    while (!_applet.isDoneAppending()) {}
		_applet.printPS(); 
	};

	service.monitorFinding = function(){	
		if (_applet != null && !_applet.isDoneFinding()) {
	    	timeout(service.monitorFinding(), 100);
		}
	};
 
	return service;
}])
.directive('axfPrint', ['$compile','$timeout','printConfig','printFactory','$parse','$q',function ($compile,timeout,printConfig,printFactory,$parse,$q) {

	var	template = '<applet id=\"{{appletId}}\" name=\"{{appletId}}\" code=\"jzebra.PrintApplet.class\"'+
					'alt=\"\" archive=\"{{urlJzebra}},{{urlPdfRender}}\"'+
					'width=\"0\" height=\"0\">'+
					'<PARAM name=\"separate_jvm\" value=\"{{separateJvm}}\"">'+
					'</applet>';
	return {		
		scope:true,
		restrict: 'A',
		link: {
				pre  : function preLink(scope, iElement,iAttrs){
					scope.urlJzebra    	= printConfig.URL_JZEBRA;
					scope.urlPdfRender 	= printConfig.URL_PDF_RENDER;	
					
					if(iAttrs.axfPrint){
						scope.printOptions = scope.$eval(iAttrs.axfPrint);						
					}

					scope.appletId 		= angular.isDefined(scope.printOptions.appletId) ? scope.printOptions.appletId : printFactory.getUniqueId();					
					
					scope.separateJvm  = 'false';
					if(angular.isDefined(iAttrs.separateJvm)){
						scope.separateJvm  = iAttrs.separateJvm;
					}
				},
				post : function postLink(scope, iElement, iAttrs) {
					var compiled = $compile(template)(scope);	

					var printPDF = function() {		
						var fn = $parse(iAttrs.printOnclick);				
                   		var returnPromise = $q.when(fn(scope));
 
 	                	returnPromise.then(function () { 	                		
	                			printFactory.printPdfTmp(scope.printOptions,scope.appletId);
	 				   		}, function (error) {
		                 		
	                		});  							
					};
																	
					timeout(function(){
						iElement.parent().append(compiled);	
						iElement.on("click", printPDF);
					}, 0);	
				}
		}
	};
}])
.directive('axfPrintPdf', ['$compile','$timeout','printConfig','printFactory','$parse','$q',function ($compile,timeout,printConfig,printFactory,$parse,$q) {

	var	template = '<applet id=\"{{appletId}}\" name=\"{{appletId}}\" code=\"jzebra.PrintApplet.class\"'+
					'alt=\"\" archive=\"{{urlJzebra}},{{urlPdfRender}}\"'+
					'width=\"0\" height=\"0\">'+
					'<PARAM name=\"separate_jvm\" value=\"{{separateJvm}}\"">'+
					'</applet>';
	return {
		
		scope:true,
		restrict: 'A',
		link: {				
				pre  : function preLink(scope, iElement,iAttrs){
					scope.urlJzebra    	= printConfig.URL_JZEBRA;
					scope.urlPdfRender 	= printConfig.URL_PDF_RENDER;		

					if(iAttrs.axfPrintPdf){
						scope.printOptions = scope.$eval(iAttrs.axfPrintPdf);						
					}

					scope.appletId 		= angular.isDefined(scope.printOptions.appletId) ? scope.printOptions.appletId : printFactory.getUniqueId();
					
					scope.separateJvm  = 'false';
					if(angular.isDefined(iAttrs.separateJvm)){
						scope.separateJvm  = iAttrs.separateJvm;
					}
				},
				post : function postLink(scope, iElement, iAttrs) {
					var compiled = $compile(template)(scope);	

					var printPDF = function() {		
						var fn = $parse(iAttrs.printOnclick);				
                   		var returnPromise = $q.when(fn(scope));
 
 	                	returnPromise.then(function () { 	                		
	                			printFactory.printPDF(scope.printOptions,scope.appletId);
	 				   		}, function (error) {
		                 		
	                		});  							
					};
									
					timeout(function(){
						iElement.parent().append(compiled);	
						iElement.on("click", printPDF);
					}, 0);	
				}
		}
	};
}])
.directive('axfPrintBgPdf', ['$compile','$timeout','printConfig',function ($compile,$timeout,printConfig) {
	var	template = '<applet id=\"{{appletId}}\" name=\"{{appletId}}\" code=\"jzebra.PrintApplet.class\"'+
					'alt=\"\" archive=\"{{urlJzebra}},{{urlPdfRender}}\"'+
					'width=\"0\" height=\"0\">'+
					'<PARAM name=\"separate_jvm\" value={{separateJvm}}>'+
					'</applet>';
	return {
		scope:true,		
		restrict: 'E',
		link: {
				pre  : function preLink(scope, iElement,iAttrs){
					scope.urlJzebra    	= printConfig.URL_JZEBRA;
					scope.urlPdfRender 	= printConfig.URL_PDF_RENDER;							

					if(angular.isDefined(iAttrs.appletId)){
						scope.appletId  = iAttrs.appletId;
					}
					else
					{
						if(angular.isDefined(scope.printOptions.appletId)){
							scope.appletId  = scope.printOptions.appletId;
						}
						else
						{
							throw 'Error axfPrintBgPdf, appletId is mandatory.';
						}
					}

					scope.separateJvm  = 'false';
					if(angular.isDefined(iAttrs.separateJvm)){
						scope.separateJvm  = iAttrs.separateJvm;
					}
				},
				post : function postLink(scope, iElement, iAttrs) {
					var compiled = $compile(template)(scope); 						

					$timeout(function() {
						iElement.append(compiled);
					},0);
				}
		} 
	};
}]);