axfDirectivesDirectives.directive('axfDisabledCopyPaste', [function () {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {

			var eventCancel = function(event) {				
				event.preventDefault();	
			};

			iElement.bind("paste cut copy drop dragover dragenter",eventCancel);
		}
	};
}]);