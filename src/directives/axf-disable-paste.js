axfDirectivesDirectives.directive('axfDisabledPaste', [function () {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) {
			var eventCancel = function(event) {
				event.preventDefault();	
			};

			iElement.bind("paste",eventCancel);
		}
	};
}]);