if (!String.prototype.trim) {
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g, '');
    };
}

// https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Array/indexOf
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (searchElement /*, fromIndex */){
    if (this === null) {
      throw new TypeError();
    }
    var t = Object(this);
    var len = t.length >>> 0;
    if (len === 0) {
      return -1;
    }
    var n = 0;
    if (arguments.length > 1) {
      n = Number(arguments[1]);
      if (n !== n) { // shortcut for verifying if it's NaN
        n = 0;
      } else if (n !== 0 && n !== Infinity && n !== -Infinity) {
        n = (n > 0 || -1) * Math.floor(Math.abs(n));
      }
    }
    if (n >= len) {
      return -1;
    }
    var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
    for (; k < len; k++) {
      if (k in t && t[k] === searchElement) {
        return k;
      }
    }
    return -1;
  };
}


function getCaretPosition(input){
    if (!input){
      return 0;
    }
    if (input.selectionStart !== undefined) {
        return input.selectionStart;
    } else if (document.selection) {
      // Curse you IE
      input.focus();
      var selection = document.selection.createRange();
      selection.moveStart('character', input.value ? -input.value.length : 0);
      return selection.text.length;
    }
    return 0;
}

function setCaretPosition(input, pos){
    if (!input) { return 0;   }
    if (input.offsetWidth === 0 || input.offsetHeight === 0) {
      return; // Input's hidden
    }
    if (input.setSelectionRange) {
      input.focus();
      input.setSelectionRange(pos, pos);
    }
    else if (input.createTextRange) {
      // Curse you IE
      var range = input.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
}

function getSelectionLength(input){
    if (!input) { return 0;}
    if (input.selectionStart !== undefined) {
      return (input.selectionEnd - input.selectionStart);
    }
    if (document.selection) {
      return (document.selection.createRange().text.length);
    }
    return 0;
}