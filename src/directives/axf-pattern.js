axfDirectivesDirectives.value('axfPatternConfig',{
    'patternDefinitions': {
      '9': /\d/,
      'A': /[a-zA-Z\s]/,
      '*': /[a-zA-Z0-9\s]/
    }
})
.directive('axfPattern', ['axfPatternConfig','$compile', function (patternConfig,$compile) {	
	return {
		restrict: 'A', 
		require : 'ngModel',
		link: function (scope, iElement, iAttrs,modelCtrl) {

			if(!modelCtrl){
				return;
			}

			if (!iElement.attr('ng-trim')){
            	iElement.attr("ng-trim", false);
            	$compile(iElement)(scope);
          	}

			var options = patternConfig,
				blackList,
				whiteList,
				selectedPattern,
				newValue = '';

			// Vars used exclusively in eventHandler()
    		var oldValue, 
    			eventsBound = false,		    	
		    	oldCaretPosition, 
		    	valAltered,
		    	oldSelectionLength;

			function initialize(pattern) {	
				selectedPattern = options.patternDefinitions[pattern];
				if(angular.isUndefined(selectedPattern)){ // custom pattern
					var tmpSelectedPattern = pattern.toString();
					if(tmpSelectedPattern.substring(0,1) === '!'){
						denyPattern = true;
						tmpSelectedPattern = pattern.replace('!','');	
					}
					selectedPattern = new RegExp(tmpSelectedPattern);

					//console.log('axfPattern->Custom selectedPattern',selectedPattern);
				}

				if(iAttrs.axfPatternBlacklist){
					blackList = iAttrs.axfPatternBlacklist.replace(',','');
				}

				if(iAttrs.axfPatternWhitelist){
					whiteList = iAttrs.axfPatternWhitelist.replace(',','');
				}
			}

			function containsInList(list,chr)
			{
				if(angular.isUndefined(list)){
					return false;
				}

				if(list.search(chr) === -1){
					return false;
				}

				return true;
			}

			iAttrs.$observe('axfPattern', function (pattern) {
				initialize(pattern);

				oldValue = '';
				oldCaretPosition = 0;	
				bindEventListeners();
			});	

			function bindEventListeners(){
	            if (eventsBound) {
	              return;
	            }
	            iElement.bind('blur', blurHandler);
	            iElement.bind('mousedown mouseup', mouseDownUpHandler);
	            iElement.bind('input keyup click focus', eventHandler);
	            eventsBound = true;
	        }

	        function blurHandler(){            
	            oldCaretPosition = 0;
	            oldSelectionLength = 0;
	        }

	        function mouseDownUpHandler(e){
	            if (e.type === 'mousedown') {
	              iElement.bind('mouseout', mouseoutHandler);
	            } else {
	              iElement.unbind('mouseout', mouseoutHandler);
	            }
	        }

	        iElement.bind('mousedown mouseup', mouseDownUpHandler);

	        function mouseoutHandler(){            
	            oldSelectionLength = getSelectionLength(this);
	            iElement.unbind('mouseout', mouseoutHandler);
	        }

			
		    function eventHandler(e){
		    	e = e || {};
	            // Allows more efficient minification
	            var eventWhich = e.which,
	            	eventType = e.type;

	            /*  8 => backspace | 9 => tab | 13 => enter | 35 => end | 36 => home | 37 => left | 39 => right | 46 => del */
				// Prevent shift and ctrl from mucking with old values
	            if (eventWhich === 16 || eventWhich === 91) { return;}

		        var val = iElement.val(),
		             	valOld = oldValue,
		            	caretPos = getCaretPosition(this) || 0,
		              	caretPosOld = oldCaretPosition || 0,
		              	caretPosDelta = caretPos - caretPosOld,
		              	selectionLenOld = oldSelectionLength || 0,
			            isSelected = getSelectionLength(this) > 0,
			            wasSelected = selectionLenOld > 0,

			            // Case: Typing a character to overwrite a selection
			            isAddition = (val.length > valOld.length) || (selectionLenOld && val.length > valOld.length - selectionLenOld),
			            // Case: Delete and backspace behave identically on a selection
			            isDeletion = (val.length < valOld.length) || (selectionLenOld && val.length === valOld.length - selectionLenOld),
			            isSelection = (eventWhich >= 37 && eventWhich <= 40) && e.shiftKey, // Arrow key codes

			            isKeyLeftArrow = eventWhich === 37,
			            // Necessary due to "input" event not providing a key code
			            isKeyBackspace = eventWhich === 8 || (eventType !== 'keyup' && isDeletion && (caretPosDelta === -1)),
			            isKeyDelete = eventWhich === 46 || (eventType !== 'keyup' && isDeletion && (caretPosDelta === 0 ) && !wasSelected);

			            oldSelectionLength = getSelectionLength(this);
		        // These events don't require any action 
		        if (isSelection || (isSelected && (eventType === 'click' || eventType === 'keyup'))) {	            	
		              return;
		        }

	   			if(valAltered)	//|| val !== newValue
				{	
					modelCtrl.$setViewValue(newValue);
					modelCtrl.$render();
				}

	  			// Caret Repositioning
	            // ===================
	            if (eventWhich === 32 && (oldCaretPosition < val.length)) {            		
	              		//caretPos = oldCaretPosition + 1;
	            }
					//console.log("oldCaretPosition",oldCaretPosition);

				oldCaretPosition = caretPos; 
	           	setCaretPosition(this, caretPos);            	

	            //console.log("caretPos",caretPos);
			}

			function parserValue(inputValue){						
				if (angular.isUndefined(inputValue)){
					oldValue = '';
					oldCaretPosition = 0;
				 	return;
				}							

				var originalValue = inputValue.toString();
				newValue = ''; // reset
				if(originalValue.length > 0){

					var arrOriginalValue = originalValue.split('');
					angular.forEach(arrOriginalValue, function (chr) {
						if((selectedPattern.test(chr) || containsInList(whiteList,chr)) && !containsInList(blackList,chr))
						{
							newValue += chr;
						}
					});					

				}
				
				valAltered = false;
				if(newValue !== inputValue){					
		        	valAltered = true;
				}

				return newValue;				
			}

			function parser(strValue){  
					
				if(angular.isUndefined(selectedPattern)){
					initialize(iAttrs.axfPattern);
					console.log("Initialize selectedPattern 'casue, it was null");
				}					

	            if(angular.isUndefined(strValue) || strValue === null){
	            	return '';
	            }
	            
				/* Fix for blanks space*/

				if(strValue.toString().trim() === iElement.val().toString().trim()){
					strValue = iElement.val();
	            }
	            
				/* End Fix for blanks space */
								
				var val = parserValue(strValue);
				
				if(angular.isUndefined(val)){ 
					val = '';
				}

				return val; //it will update the model
			}

			var minLengthValidator;
            if (iAttrs.minlength) {
                  minLengthValidator = function(value) {
	              var minlength = parseInt(iAttrs.minlength);
		          if (!modelCtrl.$isEmpty(value) && value.toString().length < minlength) {
		                modelCtrl.$setValidity('minlength', false);
		                return value;
		          } 
		          else {
		                modelCtrl.$setValidity('minlength', true);
		                return value;
		          }
           		};
        	}

        	if(angular.isDefined(minLengthValidator)){
				modelCtrl.$parsers.push(minLengthValidator);
				modelCtrl.$formatters.push(minLengthValidator);
			}

						
			modelCtrl.$parsers.unshift(parser);
			modelCtrl.$formatters.unshift(parser);
		}
	};
}]);