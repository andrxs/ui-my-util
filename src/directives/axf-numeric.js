axfDirectivesDirectives.directive('axfNumeric',['$rootScope','$log',function ($rootScope,$log) {			
	return {
		restrict: 'A',
		scope: {},
		require : 'ngModel',
		link: function (scope, iElement, iAttrs,modelCtrl) {
			if(!modelCtrl){
				return;
			}

			var cfg,
			newValue = '',			
			validChars = ['0','1','2','3','4','5','6','7','8','9'],
			containsNegSymbol = false;
			// Vars used exclusively in eventHandler()
    		var oldValue, 
    			eventsBound = false,		    	
		    	oldCaretPosition, 
		    	valAltered,
		    	oldSelectionLength; 

			function initialize(valCfg) {	
		          	cfg = angular.fromJson(valCfg) || {};

		          	/*
			          	Eg  : {"negative":true,"decimal": {"nIntegerPart": 8}}  -> Decimal
			          	Eg  : {"negative":true,"decimal": {"nIntegerPart": 8, "separator" : '.' , "nDecimalPart" : 2  }}  -> Decimal
			          	Eg  : {"negative":true,"integer": 8 }  -> Integer
		          	*/

					if(angular.isObject(cfg))
					{
						// if config.negative undefined, set to true (default is to allow negative numbers)
						if(angular.isUndefined(cfg.negative)) {
							cfg.negative = true;
						}						

						if(angular.isDefined(cfg.integer)){ // valid for only integer
							delete cfg.decimal; 
							cfg.decimal = { "separator" : '', "nIntegerPart" : cfg.integer, "nDecimalPart" : 0 }; //  not allows decimal
							return;
						}

						if(angular.isDefined(cfg.decimal)){
							if(angular.isUndefined(cfg.decimal.separator)){
								cfg.decimal.separator = $rootScope.currency.decimalSeparator;
							}
							if(angular.isUndefined(cfg.decimal.nDecimalPart)){
								cfg.decimal.nDecimalPart = $rootScope.currency.decimalLength;
							}
						}
						
						if(	angular.isUndefined(cfg.decimal.separator) 		|| 
							angular.isUndefined(cfg.decimal.nIntegerPart) 	|| 
							angular.isUndefined(cfg.decimal.nDecimalPart))
						{
							throw 'Error config axfNumeric.';
						}
					}					
			}

			var parserNumberProcessed = false;
			function parserNumber(inputValue)
			{								
				parserNumberProcessed = true;
				containsNegSymbol = false;

				if (angular.isUndefined(cfg)){
					initialize(iAttrs.axfNumeric); 
				}

				if (angular.isUndefined(inputValue)){
					oldValue = '';
					oldCaretPosition = 0;
				 	return;
				}							

				var originalValue = inputValue.toString();
				newValue = ''; // reset
				
				if(originalValue.length > 0){					
					var occurrence = (cfg.decimal.separator === '') ?  -1 : originalValue.indexOf(cfg.decimal.separator); //find position cfg.separator	

					if(occurrence === -1){ // not contains cfg.decimal.separator						
						newValue = evalNumber(originalValue , cfg.decimal.nIntegerPart); // part integer
					}				
					else{
						var tmp = originalValue.split(cfg.decimal.separator);
						newValue  = evalNumber(tmp[0] , cfg.decimal.nIntegerPart) + cfg.decimal.separator + evalNumber(tmp[1] , cfg.decimal.nDecimalPart);
					}

					//removes extra decimals characteres
					if(cfg.decimal.separator !== "")
					{
						occurrence = newValue.indexOf(cfg.decimal.separator); //find position cfg.separator						
						var searchSeparator = new RegExp('\\' + cfg.decimal.separator);

						if(occurrence !== -1){
							var decimalPart = newValue.substring(occurrence + 1);
							var intPart = newValue.substring(0,occurrence + 1);

							if(decimalPart.length > 0){
								decimalPart = decimalPart.replace(searchSeparator,'');
								newValue = intPart + decimalPart.substring(0,cfg.decimal.nDecimalPart);
							}								

							if(occurrence === 0){
								newValue = '0' + newValue; //add one zero in first place 0.[..]
							}							
						}
					}

					if(containsNegSymbol){
						newValue = '-' + newValue;
					}				
				}
				
				//valAltered = false;
				if(newValue !== inputValue){					
	            	valAltered = true;
				}

				return newValue;				
			}

			function evalNumber(inputValue,width){
				var arrOriginalValue = inputValue.split('');				
				if(cfg.negative && arrOriginalValue[0] === '-'){
						containsNegSymbol = true;
						arrOriginalValue.shift(); // remove the '-'						
				}

				var parserValue = '',
				counter = 0;
				angular.forEach(arrOriginalValue, function (chr) {	
						counter++;
						for (var i = 0; i < validChars.length; i++) {						
							if(chr === validChars[i] && counter <= width){				
								parserValue += chr;
								break;								
							}
						}
					});

				return parserValue;

			}

			function format() {
				
				if(angular.isUndefined(modelCtrl.$viewValue) || 
					(modelCtrl.$viewValue.toString() !== modelCtrl.$modelValue.toString())
				   )
					{
					modelCtrl.$setViewValue(modelCtrl.$modelValue);
					modelCtrl.$render();
				}

				if(cfg.decimal.separator === ''){ 
					return modelCtrl.$viewValue; // integer
				}

				if(modelCtrl.$viewValue === '' || angular.isUndefined(modelCtrl.$viewValue)){
					return;
				}

				var formatVal = toFixed(modelCtrl.$viewValue, cfg.decimal.nDecimalPart);

				if(modelCtrl.$viewValue.toString() !== formatVal){
					modelCtrl.$setViewValue(formatVal);
					modelCtrl.$render();
				}

				return formatVal;
			}

			function toFixed(value,width){			  
				if(angular.isUndefined(width) ||
				   angular.isUndefined(value)){
					return;
				}			

				value  = value.toString();
				var occurrence = value.indexOf(cfg.decimal.separator); //find position cfg.separator	

				if(occurrence === -1){
					return value + cfg.decimal.separator + new Array(width + 1).join('0');
				}

				var decimalPart = value.substring(occurrence + 1);
				var intPart = value.substring(0, occurrence + 1);

				width -= decimalPart.length;

				if ( width > 0 ){			  
				    decimalPart = decimalPart + new Array(width+1).join('0');
				}
				  
				return intPart + decimalPart;  
			}

			var minValidator;
            if (iAttrs.min) {
                  minValidator = function(value) {
	              var min = parseFloat(iAttrs.min);
		          if (!modelCtrl.$isEmpty(value) && value < min) {
		                modelCtrl.$setValidity('min', false);
		                return undefined;
		          } 
		          else {
		                modelCtrl.$setValidity('min', true);
		                return value;
		          }
           		};
        	}

			var maxValidator;
            if (iAttrs.max) {
				maxValidator = function(value) {
					var max = parseFloat(iAttrs.max);
					
					if (!modelCtrl.$isEmpty(value) && value > max) {
					    modelCtrl.$setValidity('max', false);
					    return undefined;
					}
					else {
					    modelCtrl.$setValidity('max', true);
					    return value;
					}	            
				};
			}

			iAttrs.$observe('axfNumeric', function (valCfg) {
				try{
					initialize(valCfg);
					if(cfg.decimal.separator.trim().length > 0){
						validChars.push(cfg.decimal.separator);
					}					

					oldValue = '';
					oldCaretPosition = 0;					
					bindEventListeners();
				
				}catch (e) {		          
		           $log.error("Error in axfNumeric, review the config. " + e); 
		        }							
			});	

			if(angular.isDefined(maxValidator)){
				modelCtrl.$parsers.push(maxValidator);
				modelCtrl.$formatters.push(maxValidator);
			}

			if (angular.isDefined(minValidator)){
				modelCtrl.$parsers.push(minValidator);
	        	modelCtrl.$formatters.push(minValidator);
	    	}
 
		    function bindEventListeners(){
	            if (eventsBound) {
	              return;
	            }
	            iElement.bind('blur', blurHandler);
	            iElement.bind('mousedown mouseup', mouseDownUpHandler);
	            iElement.bind('input keyup click focus', eventHandler);
	            eventsBound = true;
	        }

	        function unbindEventListeners(){
	            if (!eventsBound) {
	              return;
	            }

	            iElement.unbind('blur', blurHandler);
	            iElement.unbind('mousedown', mouseDownUpHandler);
	            iElement.unbind('mouseup', mouseDownUpHandler);
	            iElement.unbind('input', eventHandler);
	            iElement.unbind('keyup', eventHandler);
	            iElement.unbind('click', eventHandler);
	            iElement.unbind('focus', eventHandler);
	            eventsBound = false;
	        }

	        function blurHandler(){            
	            oldCaretPosition = 0;
	            oldSelectionLength = 0;           
	            if(angular.isDefined(modelCtrl.$modelValue)){
					format();	
				}
	        }

	        function mouseDownUpHandler(e){
	            if (e.type === 'mousedown') {
	              iElement.bind('mouseout', mouseoutHandler);
	            } else {
	              iElement.unbind('mouseout', mouseoutHandler);
	            }
	        }

	        iElement.bind('mousedown mouseup', mouseDownUpHandler);
	        function mouseoutHandler(){
	            oldSelectionLength = getSelectionLength(this);
	            iElement.unbind('mouseout', mouseoutHandler);
	        }

		    function eventHandler(e){
		    	e = e || {};
	            // Allows more efficient minification
	            var eventWhich = e.which,
	            	eventType = e.type;

	            /*  8 => backspace | 9 => tab | 13 => enter | 35 => end | 36 => home | 37 => left | 39 => right | 46 => del */
				// Prevent shift and ctrl from mucking with old values
	            if (eventWhich === 16 || eventWhich === 91) { return;}

		            var val = iElement.val(),
		             	valOld = oldValue,
		            	caretPos = getCaretPosition(this) || 0,
		              	caretPosOld = oldCaretPosition || 0,
		              	caretPosDelta = caretPos - caretPosOld,
		              	selectionLenOld = oldSelectionLength || 0,
			            isSelected = getSelectionLength(this) > 0,
			            wasSelected = selectionLenOld > 0,

			            // Case: Typing a character to overwrite a selection
			            isAddition = (val.length > valOld.length) || (selectionLenOld && val.length > valOld.length - selectionLenOld),
			            // Case: Delete and backspace behave identically on a selection
			            isDeletion = (val.length < valOld.length) || (selectionLenOld && val.length === valOld.length - selectionLenOld),
			            isSelection = (eventWhich >= 37 && eventWhich <= 40) && e.shiftKey, // Arrow key codes

			            isKeyLeftArrow = eventWhich === 37,
			            // Necessary due to "input" event not providing a key code
			            isKeyBackspace = eventWhich === 8 || (eventType !== 'keyup' && isDeletion && (caretPosDelta === -1)),
			            isKeyDelete = eventWhich === 46 || (eventType !== 'keyup' && isDeletion && (caretPosDelta === 0 ) && !wasSelected);

			            oldSelectionLength = getSelectionLength(this);

		            // These events don't require any action
		            if (isSelection || (isSelected && (eventType === 'click' || eventType === 'keyup'))) {	            	
		              return;
		            }
		           
	   				if(valAltered || val !== newValue)
					{							
						modelCtrl.$setViewValue(newValue);
						modelCtrl.$render();
					}

	  				// Caret Repositioning
	            	// ===================
	            	if (eventWhich === 32 && (oldCaretPosition < val.length)) {            		
	              		//caretPos = oldCaretPosition + 1;
	            	}
					//console.log("oldCaretPosition",oldCaretPosition);

				 	oldCaretPosition = caretPos; 
	           		setCaretPosition(this, caretPos);            	

	            	//console.log("caretPos",caretPos);
				}

				function parser(strValue){        
	            	if(angular.isUndefined(strValue)){
	            		return '';
	            	}

					var number = parserNumber(strValue);
					if(angular.isUndefined(number)){ 
						number = '';
					}

					return number; //it will update the model
				}

				function formatter(value) {													
					if(angular.isDefined(value) && value !== null){			
						return format();
					}
				}
						
			modelCtrl.$parsers.unshift(parser);			
			modelCtrl.$formatters.unshift(formatter);
		}
	};
}]);

