axfDirectivesDirectives.directive('axfSinput', function ($compile) {	  
    return {
      restrict: 'A',
      require: ['^rcSubmit'],
      terminal: true,
      priority: 1000,
      scope: true,
      compile: function compile(element, attrs) {
        element.removeAttr('axf-sinput');      
        element.removeAttr("data-axf-sinput");
        element.removeAttr("custom-operator");
        element.removeAttr("data-custom-operator");
        return {
          pre: function preLink(scope, iElement, iAttrs) { 
          },
          post: function postLink(scope, iElement, iAttrs) {        
              var tplValidator = '<small class="help-block"  ng-show="({{formName}}.{{name}}.$error.{{validators[%]}} && rc.{{formName}}.attempted) '+
                      '  #customOperator#">{{messages[%]}}</small>';    		        			 
          		var i = 0;
          		var tmp = "";
          		scope.messages = [];
          		scope.validators = [];    
	          	angular.forEach(scope.controls, function (ctrl,key) {
    	          	if(iAttrs.id === ctrl.name)
    					    {
    			          		iElement.attr('name', ctrl.name);
    			          		scope.name = ctrl.name;
                       
                        if(iAttrs.customOperator){
                          tplValidator = tplValidator.replace('#customOperator#',' || ' + iAttrs.customOperator);  
                        }
                        else{
                          tplValidator = tplValidator.replace('#customOperator#','');  
                        }

    			          		// add attribute  into element
    			          		angular.forEach(ctrl.attr, function (attrVal,attr) {
    			          			iElement.attr(attr, attrVal); 
    			          		});

                        //if(angular.isDefined(ctrl.messages)){
        			          		// add messages for validatorsvalidatorName
        			          		angular.forEach(ctrl.messages, function (message,validatorName) {
        			          		if(message.trim().length > 0){
        			          				tmp += tplValidator.replace(/%/g,i++);
                                
        			          				scope.validators.push(validatorName);
        			          				scope.messages.push(message);								
        			          			}
        			          		});
                        //}
                        //if(tmp.trim().length > 0){
                          iElement.parent().append($compile(tmp)(scope));    
                        //}
                        
    			          		return false; //break forEach
       		       }
	 			     });

            	$compile(iElement)(scope); 
              if (!scope.$$phase){
                scope.$apply();
              } 
          } 
        };
      }
    };
  });

