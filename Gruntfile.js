module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
    clean: ["dist"],
    srcFiles: ['src/*.js','src/directives/*.js'],
    removelogging: {
        dist: {
          src: "dist/<%= pkg.name %>-<%=pkg.version%>.js" // Each file will be overwritten with the output!
        }
    },
    concat: {
      options: {
            // Replace all 'use strict' statements in the code with a single one at the top
            banner: '<%= banner %>'+ '\'use strict\';\n',
            stripBanners: true,
            process: function(src, filepath) {
              return '// Source: ' + filepath + '\n' +
                src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
			       },
      },
      rc: {
      		  options: {
                  // Replace all 'use strict' statements in the code with a single one at the top
                  banner: '<%= banner %>'+ '\'use strict\';\n',
                  stripBanners: true,
                  process: function(src, filepath) {
                    return '// Source: ' + filepath + '\n' +
                      src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1').replace('debugger;','');
      			     },
            },
            src: ['<%=srcFiles%>'],
            dest: 'dist/<%= pkg.name %>-<%=pkg.version%>.js',
      },
	  debug: {
          src: ['<%=srcFiles%>'],
          dest: 'dist/<%= pkg.name %>-<%=pkg.version%>-debug.js',
      }
    },
    uglify: {
      options: {
        banner :'<%= banner %>',
        compress: {
          drop_console: true,
          global_defs: {
            "DEBUG": false
          },
          dead_code: true
        }
      },
      my_target: {
        files: {
          'dist/<%=pkg.name%>-<%=pkg.version%>.min.js': ['dist/<%=pkg.name%>-<%=pkg.version%>.js']
        }
      }
    },
    jshint: {
      options: {
                reporter: require('jshint-stylish'),
                boss: true,
                browser: true,
                camelcase: true,
                curly: true,
                eqeqeq: true,
                eqnull: true,
                // forin: true,
                immed: true,
                // indent: 4,
                latedef: true,
                // newcap: true,
                noarg: true,
                sub: true,
                // undef: true,
                // unused: true,
                globals: {
                    angular: false,
                    $: false
                }
      },
      ignore_warning: {
                 options: {
                     '-W087': false,
                 },
                 src: ['<%=srcFiles%>'],
      },
      beforeconcat: ['<%=srcFiles%>']
      //,afterconcat: ['dist/<%=pkg.name%>-<%=pkg.version%>.js']
    },
    watch: {
      gruntfile: {
        files: 'Gruntfile.js',
        tasks: ['jshint'],
      },
      src: {
        files: ['<%=srcFiles%>'],
        tasks: ['default'],
      }     
  },
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');  
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks( "grunt-remove-logging" );
  grunt.loadNpmTasks('grunt-contrib-watch');


  // Default task(s).
  grunt.registerTask('default', ['jshint','clean','concat','uglify','removelogging']);

};