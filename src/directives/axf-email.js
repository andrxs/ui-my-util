axfDirectivesDirectives.directive('axfEmail', [function () {
	return {
		restrict: 'A',
		require : 'ngModel',
		link: function (scope, iElement, iAttrs,modelCtrl) {
			if(!modelCtrl){
				return;
			}

     		var EMAIL_REGEXP = /^[a-z0-9!#$%&'\"*+/=?^_`{|}~.-]+@[a-z0-9-]+[\.][a-z0-9-]+(?:\.[a-z0-9-]+)$/;

     		var emailValidator = function(value) {
	     		if (!value || EMAIL_REGEXP.test(value)) {
		     		  modelCtrl.$setValidity('email', true);
		     		  return value;
		     		} else {
		     		  modelCtrl.$setValidity('email', false);
		     		  return undefined;
		     		}
     		};

     		modelCtrl.$parsers.push(emailValidator);
     		modelCtrl.$formatters.push(emailValidator);
		}
	};
}]);